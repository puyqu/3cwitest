package at.emze.Musicbox;

import java.util.ArrayList;

/**
 * @author Emanuel Zech
 * <p>
 * created on 01.06.2021
 */
public class Titles {
    private int length;
    private int id;
    private String name;



    public Titles(int length, int id, String name) {
        this.length = length;
        this.id = id;
        this.name = name;

    }



    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


