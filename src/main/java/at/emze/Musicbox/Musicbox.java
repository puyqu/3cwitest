package at.emze.Musicbox;

/**
 * @author Emanuel Zech
 * <p>
 * created on 01.06.2021
 */
public class Musicbox {

    private Records records;
    private Titles titles;

    public Musicbox() {

    }

    public Musicbox(Records records) {
        this.records = records;
    }

    public void play(int num){
        getRecords().play(num);

    }
    public void loadRecord(Records records){
        Musicbox musicbox = new Musicbox(records);
    }

    public void getSumOfMusic() {
        getRecords().getSumOfMusic();
    }

    public void addRecord(Records records) {
        getRecords().saveRecord(records);
    }

    public void removeRecords(Records records) {
        removeRecords(records);
    }

    public Records getRecords() {
        return records;
    }

    public void setRecords(Records records) {
        this.records = records;
    }

    public Titles getTitles() {
        return titles;
    }

    public void setTitles(Titles titles) {
        this.titles = titles;
    }
}
