package at.emze.Musicbox;

import java.util.ArrayList;

/**
 * @author Emanuel Zech
 * <p>
 * created on 01.06.2021
 */
public class Records {
    private String name;
    private int id;

    int numbOfRecords = 0;
    private ArrayList<Titles> titles = new ArrayList<Titles>();
    private Titles title;
    private ArrayList<Records> records = new ArrayList<Records>();
    private Records record;



    public Records(String name, int id) {
        this.name = name;
        this.id = id;



    }
    public void play(int num){
        System.out.println(titles.get(num + 1).getName() + " wird gespielt");
    }
    public void saveRecord(Records records){
        if(numbOfRecords <= 50){
            this.record = records;
            this.records.add(records);
            numbOfRecords++;
        }
        else{
            System.out.println("Musikbox ist voll!");
        }
    }

    public void deleteRecord(Records records){
        this.records.remove(records);
        numbOfRecords--;
    }
    public void getSumOfMusic(){
        for (int i = 0; i < records.size(); i++) {
            for (int j = 0; j < titles.size(); j++) {
                int a = 0;
                a = a +titles.get(j).getLength();
            }
        }
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumbOfRecords() {
        return numbOfRecords;
    }

    public void setNumbOfRecords(int numbOfRecords) {
        this.numbOfRecords = numbOfRecords;
    }



    public ArrayList<Records> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<Records> records) {
        this.records = records;
    }

    public Records getRecord() {
        return record;
    }

    public void setRecord(Records record) {
        this.record = record;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Titles> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<Titles> titles) {
        this.titles = titles;
    }

    public Titles getTitle() {
        return title;
    }

    public void setTitle(Titles title) {
        this.title = title;
    }
}
