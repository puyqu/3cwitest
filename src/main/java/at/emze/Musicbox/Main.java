package at.emze.Musicbox;

import java.util.ArrayList;

/**
 * @author Emanuel Zech
 * <p>
 * created on 01.06.2021
 */
public class Main {

    public static void main(String[] args) {
        Musicbox musicbox = new Musicbox();

        Records record1 = new Records("Record1", 1);
        Records record2 = new Records("Record2", 2);
        musicbox.addRecord(record1);
        musicbox.addRecord(record2);
        record1.getTitles().add(new Titles(210, 1, "Song1"));
        record1.getTitles().add(new Titles(180, 2, "Song2"));
        record1.getTitles().add(new Titles(190, 3, "Song3"));
        record2.getTitles().add(new Titles(160, 1, "Song1"));
        record2.getTitles().add(new Titles(170, 2, "Song2"));

        musicbox.loadRecord(record1);
        musicbox.getSumOfMusic();
        musicbox.play(2);
    }
}
